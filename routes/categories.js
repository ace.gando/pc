const express = require('express');
const router = express.Router();
const passport = require('passport');

const CategoryModel = require('../models/Categories');

// index
router.get('/',function(req,res,next){
	// res.json({message: 'This is ironman'})
	CategoryModel.find()
	.then(categories => {
		res.json(categories)
	})
	.catch(next)
});

// single
router.get('/:id', (req,res,next) => {
	// res.json({
	// 	data : req.params.id
	// });
	CategoryModel.findOne({_id : req.params.id})
	.then(category => res.json(category))
	.catch(next)
});

// create
router.post('/', passport.authenticate('jwt',{session : false}), (req,res,next) => {

	if (req.user.role !== 'admin') {
		return res.send({message : "Unauthorized request"})
	}

	// CategoryModel.create({ name: 'small' }, function (err, small) {
 //  		console.log(small);
 //  		res.send(small)
	// });

	CategoryModel.create({
		name : req.body.name
	})
	.then((category) => {
		res.send(category)
	})
	.catch(next)

	// res.json({
	// 	data : req.body
	// })
})

// update
router.put('/:id', (req,res,next) => {
	// res.json({
	// 	data : "This is a put request",
	// 	id : req.params.id,
	// 	body : req.body
	// })
	CategoryModel.findOneAndUpdate(
		{
			_id : req.params.id
		},
		{
			name : req.body.name
		},
		{
			new : true
		}
	)
	.then(category => res.json(category))
	.catch(next)
})

// delete
router.delete('/:id', (req,res,next) => {
	// res.json({
	// 	data : "this is a delete request",
	// 	id : req.params.id + " to be deleted"
	// })
	CategoryModel.findOneAndDelete({_id : req.params.id})
	.then(category => res.json(category))
	.catch(next)
})

module.exports = router;